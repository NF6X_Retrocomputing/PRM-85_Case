# PRM-85 Case

A 3D-printable case for [Bill Kotaska's PRM-85 board](http://vintagecomputers.site90.net/hp85/prm85.htm).

![Exploded Diagram](PRM-85_Case_Exploded.png)

